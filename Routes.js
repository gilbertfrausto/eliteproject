import React from 'react';
import { Router, Scene } from 'react-native-router-flux';
import Home from './routes/Home';
import Intro from './routes/Intro';
import WeekOne from './routes/WeekOne';
import {Text, View, StyleSheet} from 'react-native';

const Routes = () => (
  <>  
  <Router>
    <Scene key="root">
      <Scene key="home" component={Home} title="Home" initial={true} />
      <Scene key="intro" component={Intro} title="Intro"/>
      <Scene key="weekOne" component={WeekOne} title="WeekOne"/>
    </Scene>
  </Router>
  </>
)
export default Routes