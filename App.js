import React, {useState} from 'react';
import {Text, View, StyleSheet, AppRegistry} from 'react-native';
import Routes from './Routes';
import Header from './components/Header';
import Nav from './components/Nav';
import Content from './components/Content';
import {addTransiton} from './styles/styles';
import {CONSTANTS} from './constants';

export default class App extends React.Component {
  constructor() {
    super();
    this.state = { week: 0 }
    this.updateState = (e) => this.stateUpdater(e);
  }

  /**
   * Updates steps and add transiton to the event target
   * @param {Event} event 
   * @return {void}
   */
  stateUpdater(event) {
    const {target} = event;
    
    // Add transiton proptery to event target(StyleSheet.create bug)
    addTransiton(target);
    
    // Update state
    this.setState({
      week: Number(target.getAttribute(CONSTANTS.WEEK))
    });
  }

  /**
   * @return {JSX.Element}
   */
  render() {
    return (
      <View style={styles.main}>
        <Header updateState = {this.updateState} />

        {/* Navigation to each week */}
        <Nav week = {this.state.week} updateState = {this.updateState} />

        {/* Contains components for each step */}
        <Content week = {this.state.week} updateState = {this.updateState}  />
      </View>
    )
  }
}

const styles = StyleSheet.create({
  main: {
    backgroundColor: '#161524',
    height: '100%',
  },
  text: {
    paddingTop: '50px',
    paddingLeft: '50px',
    color: 'white'
  }
});

AppRegistry.registerComponent("EliteApp", () => App);