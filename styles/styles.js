import { StyleSheet } from 'react-native'

/**
 * Add completion styles to step text
 * @return {void}
 */
export const updateStepStyle = () => {
  const {target} = event;
  target.style.transition = 'all .5s ease';
  target.style.textDecoration = 'line-through';
  target.style.color = '#3e3e3e';
}

/**
 * Adds css trasiton property to event target
 * @param {Event} target 
 * @return {void}
 */
export const addTransiton = (target) => {
  target.style.transition = 'all .5s ease-in-out';
}

// Util styles
export const utilityStyles = StyleSheet.create({
  buttonPrimary: {
    backgroundColor: 'transparent',
    color: 'white',
    paddingTop: '15px',
    paddingBottom: '15px',
    width: '257px',
    textAlign: 'center',
    minHeight: '53px',
    fontWeight: '600'
  },
  buttonSecondary: {
    backgroundColor: 'transparent',
    color: 'black',
    paddingTop: '15px',
    paddingBottom: '15px',
    width: '257px',
    textAlign: 'center',
    minHeight: '53px',
    fontWeight: '600'
  },
  buttonDisabled: {
    backgroundColor: 'transparent',
    color: 'black',
    paddingTop: '15px',
    paddingBottom: '15px',
    width: '257px',
    textAlign: 'center',
    minHeight: '53px',
    fontWeight: '600',
    pointerEvents: 'none'
  },
  buttonContainer: {
    width: '100%',
    alignItems: 'center',
    marginBottom: '50px'
  },
  fill: {
    height: 'auto',
    display: 'flex',
    flexGrow: Number('1'),
    flexShrink: Number('1'),
    flexBasis: 'auto'
  },
  contentFill: {
    height: 'auto',
    display: 'flex',
    flexGrow: Number('1'),
    flexShrink: Number('1'),
    flexBasis: 'auto',
    justifyContent: 'space-between'
  },
  text: {
    color: 'white',
    width: '97%',
    paddingLeft: '30px',
    paddingBottom: '20px',
    fontWeight: '600',
    // transition: 'all .5s ease'
  },
  stepComplete: {
    // textDecoration: 'lineThrough',
    color: 'gray'
  },
  coloredText: {
    color: '#05b3b8',
    fontWeight: '800'
  },
  stepsContainer: {
    width: '85%',
    margin: '0 auto',
    display: 'flex',
    alignItems: 'center'
  },
  padTopSM: { paddingTop: '10px'},
  padTopMd: { paddingTop: '20px'},
  padTopL: { paddingTop: '30px'},
  padTopXL: { paddingTop: '50px'}
})