import React from 'react'
import { StyleSheet, Text, View, Button } from 'react-native'
import { utilityStyles } from '../styles/styles'

export default class Intro extends React.Component {
  constructor(props) {
    super();
  }
  render() {
    return (
      <View style={ utilityStyles.contentFill }>
        <View>
          <Text style = { styles.header }>Welcome to the 10-week Breathing Program</Text>
          <Text style = { styles.intro }>
            Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut 
            labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation 
            ullamco laboris nisi ut aliquip ex ea commodo consequat.
          </Text>
          <Text style = { styles.intro }>
            Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor
            incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam
          </Text>
          <Text style = { styles.intro }>
            Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor
            incididunt ut labore et dolore magna aliqua.
          </Text>
        </View>

        {/* <Button backgroundColor='white' title='asdf'/> */}


        <View style={utilityStyles.buttonContainer}>
          <Text style={utilityStyles.buttonPrimary} onStartShouldSetResponder = {(e) => this.props.updateState(e)} week="1">BEGIN PROGRAM</Text>
          <Text style={utilityStyles.buttonSecondary}>ORDER ON AMAZON</Text>
        </View>
        
      </View>
    )
  }
}

const styles = StyleSheet.create({
  header: {
    color: 'white',
    fontSize: '15px',
    width: '100%',
    textAlign: 'center',
    paddingTop: '20px',
    paddingBottom: '30px',
    fontWeight: '700'
  },
  intro: {
    color: 'white',
    width: '97%',
    paddingLeft: '30px',
    paddingBottom: '20px',
    fontWeight: '500'
  }
});
