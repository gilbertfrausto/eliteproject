import React from 'react';
import { TouchableOpacity, Text, StyleSheet } from 'react-native';
import { Actions } from 'react-native-router-flux';


export default class Home extends React.Component {
  goToAbout() {
    Actions.intro()
  }
  render() {
    return (
      <TouchableOpacity style = {{ margin: 128 }} onPress = {this.goToAbout}>
        <Text>This is intro</Text>
      </TouchableOpacity>
    )
  }
}

const styles = StyleSheet.create({
  main: {
    backgroundColor: '#161524',
    height: '100%', 
  },
  text: {
    paddingTop: '50px',
    paddingLeft: '50px',
    color: 'white'
  }
});