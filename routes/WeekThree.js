import React from 'react'
import { StyleSheet, Text, View } from 'react-native'
import { Actions } from 'react-native-router-flux'

export default class WeekThree extends React.Component {
  render() {
    return (
      <View>
        <Text style = { styles.weekThree }>Week Three</Text>
      </View>
    )
  }
}

const styles = StyleSheet.create({
  weekThree: {
    color: 'white'
  },
});
