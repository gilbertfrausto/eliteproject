import React from 'react'
import { StyleSheet, Text, View, Animated } from 'react-native'
import { utilityStyles, updateStepStyle } from '../styles/styles'
import { CONSTANTS } from '../constants';

export default class WeekOne extends React.Component {

  constructor() {
    super();

    // Completion flags for each step
    this.steps = {
      step_1: false,
      step_2: false,
      step_3: false
    }
    

    // State
    this.state = {
      stepCount: 0,
      fadeValue: new Animated.Value(0)
    }

    // Fade in view
    this.startAnimaiton();
  }

  /**
   * Complete a step handler
   * @param {Event} event 
   * @return {void}
   */
  complete(event) {
    const {target} = event;
    
    // Adds completion styles to the text
    updateStepStyle(event);

    const stepNumber = target.getAttribute(CONSTANTS.STEP);

    // Check if step is completed or not.
    if (!this.steps[stepNumber]) {
      this.steps[stepNumber] = true;
      
      // Update state step counter.
      this.setState({
        stepCount: this.state.stepCount + 1
      });
    }
  }

  /**
   * Go to the next only if the current week's steps are cleared
   * @param {Event} event 
   * @return {void}
   */
  goToNext(event) {
    if (this.state.stepCount >= 3) {
      this.props.updateState(event);
    }
  }

  /**
   * Start fade in animaitons
   * @return {void}
   */
  startAnimaiton(){
    Animated.timing(this.state.fadeValue, {
      toValue: 1,
      duration: 1000
    }).start();
  }

  /**
   * @return {JSX.Element}
   */
  render() {
    return (
      <Animated.View  style={{opacity: this.state.fadeValue, height: '100%'}}>
        <View style={ utilityStyles.contentFill }>
          <View style={ utilityStyles.padTopL }>
            
            {/* Header */}
            <Text style = { utilityStyles.text }>
              Lorem ipsum dolor sit amet, consectetur adipiscing elit, 
              sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. 
              Ut enim ad minim veniam.
            </Text>

            {/* STEPS */}
            <View style = { utilityStyles.stepsContainer, utilityStyles.padTopL} onStartShouldSetResponder = {(e) => this.complete(e)}>
              
              {/* Step 1 */}
              <Text style = { utilityStyles.text } step="step_1">
                Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et. 
                <Text style = { utilityStyles.coloredText }> Learn How</Text>
              </Text>
              
              {/* Step 2 */}
              <Text style = { utilityStyles.text } step="step_2">
                Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor
              </Text>

              {/* Step 3 */}
              <Text style = { utilityStyles.text } step="step_3">
                Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor
              </Text>
            </View>
          </View>

          {/* CTA buttons */}
          <View style = { utilityStyles.buttonContainer }>
            <Text style = { (this.state.stepCount >= 3) ? utilityStyles.buttonPrimary : utilityStyles.buttonSecondary } onStartShouldSetResponder = {(e) => this.goToNext(e)} week="2">
              UNLOCK WEEK 2
            </Text>
          </View>
          
        </View>
      </Animated.View>
    )
  }
}

const styles = StyleSheet.create({
  weekOne: {
    color: 'white'
  },
});
