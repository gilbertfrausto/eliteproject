import React from 'react'
import { StyleSheet, Text, View } from 'react-native'
import { Actions } from 'react-native-router-flux'

export default class WeekSix extends React.Component {
  render() {
    return (
      <View>
        <Text style = { styles.weekThree }>Week Six</Text>
      </View>
    )
  }
}

const styles = StyleSheet.create({
  weekSix: {
    color: 'white'
  },
});
