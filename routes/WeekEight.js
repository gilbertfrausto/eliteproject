import React from 'react'
import { StyleSheet, Text, View } from 'react-native'
import { Actions } from 'react-native-router-flux'

export default class WeekEight extends React.Component {
  render() {
    return (
      <View>
        <Text style = { styles.weekThree }>Week Eight</Text>
      </View>
    )
  }
}

const styles = StyleSheet.create({
  weekEight: {
    color: 'white'
  },
});
