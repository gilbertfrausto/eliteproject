import React from 'react'
import { StyleSheet, Text, View } from 'react-native'
import { Actions } from 'react-native-router-flux'

export default class WeekSeven extends React.Component {
  render() {
    return (
      <View>
        <Text style = { styles.weekThree }>Week Seven</Text>
      </View>
    )
  }
}

const styles = StyleSheet.create({
  weekSeven: {
    color: 'white'
  },
});
