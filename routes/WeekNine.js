import React from 'react'
import { StyleSheet, Text, View } from 'react-native'
import { Actions } from 'react-native-router-flux'

export default class WeekNine extends React.Component {
  render() {
    return (
      <View>
        <Text style = { styles.weekThree }>Week Nine</Text>
      </View>
    )
  }
}

const styles = StyleSheet.create({
  weekNine: {
    color: 'white'
  },
});
