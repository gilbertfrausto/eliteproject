import React, {useState} from 'react'
import {Text, View, StyleSheet} from 'react-native';

const printf = console.log;

const t = (e) => {
  printf('works', e.target)
}
export default class Nav extends React.Component {
  constructor(props) {
    super();
  }
  render() {
    return (
      <View style={styles.nav} onStartShouldSetResponder = {(e) => this.props.updateState(e)}>
        <Text style={styles.linkActive} week="0">Intro</Text>
        <Text style={(this.props.week >= 1) ? styles.linkActive : styles.linkInactive} week="1">Week 1</Text>
        <Text style={(this.props.week >= 2) ? styles.linkActive : styles.linkInactive} week="2">Week 2</Text>
        <Text style={(this.props.week >= 3) ? styles.linkActive : styles.linkInactive} week="3">Week 3</Text>
        <Text style={(this.props.week >= 4) ? styles.linkActive : styles.linkInactive} week="4">Week 4</Text>
        <Text style={(this.props.week >= 5) ? styles.linkActive : styles.linkInactive} week="5">Week 5</Text>
        <Text style={(this.props.week >= 6) ? styles.linkActive : styles.linkInactive} week="6">Week 6</Text>
        <Text style={(this.props.week >= 7) ? styles.linkActive : styles.linkInactive} week="7">Week 7</Text>
        <Text style={(this.props.week >= 8) ? styles.linkActive : styles.linkInactive} week="8">Week 8</Text>
        <Text style={(this.props.week >= 9) ? styles.linkActive : styles.linkInactive} week="9">Week 9</Text>
        <Text style={(this.props.week >= 10) ? styles.linkActive : styles.linkInactive} week="10">Week 10</Text>
      </View>      
    )
  }
}

const styles = StyleSheet.create({
  nav: {
    paddingLeft: '15px',
    display: 'flex',
    flexDirection: 'row',
    width: '100%',
    overflow: 'scroll'
  },
  linkActive: {
    color: '#fff',
    padding: '10px',
    cursor: 'pointer',
    marginLeft: '5px',
    marginRight: '5px',
    whiteSpace: 'nowrap',
    fontSize: '16px'
  },
  linkInactive: {
    color: 'gray',
    padding: '10px',
    cursor: 'pointer',
    marginLeft: '5px',
    marginRight: '5px',
    whiteSpace: 'nowrap',
    fontSize: '16px'
  }
});


