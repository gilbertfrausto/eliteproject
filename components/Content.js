import React from 'react'
import {Text, View, StyleSheet, Button} from 'react-native';
import Intro from '../routes/Intro';
import WeekOne from '../routes/WeekOne';
import WeekTwo from '../routes/WeekTwo';
import WeekThree from '../routes/WeekThree';
import WeekFour from '../routes/WeekFour';
import WeekFive from '../routes/WeekFive';
import WeekSix from '../routes/WeekSix';
import WeekSeven from '../routes/WeekSeven';
import WeekEight from '../routes/WeekEight';
import WeekNine from '../routes/WeekNine';
import WeekTen from '../routes/WeekTen';
import {utilityStyles} from '../styles/styles';

export default class Content extends React.Component {
  constructor(props) {
    super();
  }
  
  /**
   * @return {JSX.Element}
   */
  render() {
    return (
      <View style={ utilityStyles.fill }>
        {this.props.week === 0 && <Intro updateState = {this.props.updateState} />}
        {this.props.week === 1 && <WeekOne updateState = {this.props.updateState} />}
        {this.props.week === 2 && <WeekTwo updateState = {this.props.updateState} />}
        {this.props.week === 3 && <WeekThree updateState = {this.props.updateState} />}
        {this.props.week === 4 && <WeekFour updateState = {this.props.updateState} />}
        {this.props.week === 5 && <WeekFive updateState = {this.props.updateState}/>}
        {this.props.week === 6 && <WeekSix updateState = {this.props.updateState}/>}
        {this.props.week === 7 && <WeekSeven updateState = {this.props.updateState} />}
        {this.props.week === 8 && <WeekEight updateState = {this.props.updateState} />}
        {this.props.week === 9 && <WeekNine updateState = {this.props.updateState}/>}
        {this.props.week === 10 && <WeekTen updateState = {this.props.updateState}/>}
      </View>
    )
  }
}
