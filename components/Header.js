import React from 'react'
import {Text, View, StyleSheet, Button} from 'react-native';

export default class Header extends React.Component {
  render() {
    return (
      <>
        <Text style={styles.button} onStartShouldSetResponder = {(e) => this.props.updateState(e)} week="0">Exit</Text>
        
        <View style={styles.wrapper}>
          <Text style={styles.header}>
            10 Week Breathing Program
          </Text>
        </View>
      </>
    )
  }
}

const styles = StyleSheet.create({
  wrapper: {
    paddingTop: '20px',
    paddingLeft: '50px',
    paddingBottom: '50px;'
  },
  header: {
    fontWeight: 'bold',
    color: 'white',
    display: 'flex',
    textAlign: 'left',
    justifyContent: 'center',
    fontSize: '25px',
    width: '75%',
  },
  button: {
    paddingTop: '50px',
    paddingLeft: '25px',
    color: 'white',
    cursor: 'pointer'
  }
});
